﻿namespace StudentManagement
{
    public class StudentManager
    {
        private StudentContext db;

        public StudentManager(StudentContext context)
        {
            db = context;
        }

        public void AddStudent()
        {
            Console.Clear();
            Console.WriteLine("Add Student");
            Console.Write("Enter name: ");
            string name = Console.ReadLine();
            Console.Write("Enter age: ");
            int age = Convert.ToInt32(Console.ReadLine());
            Console.Write("Enter major: ");
            string major = Console.ReadLine();
            Console.Write("Enter gender (M/F): ");
            string gender = Console.ReadLine();
            var student = new Student { Name = name, Age = age, Major = major, Gender = gender };
            db.Students.Add(student);
            db.SaveChanges();
            Console.WriteLine("Student added successfully!");
            Console.ReadKey();
        }

        public void ViewStudents()
        {
            Console.Clear();
            Console.WriteLine("View Students");
            var students = db.Students.ToList();
            foreach (var student in students)
            {
                Console.WriteLine("{0} - {1} - {2} - {3}", student.Name, student.Age, student.Major, student.Gender);
            }
            Console.ReadKey();
        }

        public void EditStudent()
        {
            Console.Clear();
            Console.WriteLine("Edit Student");
            Console.Write("Enter student id: ");
            int id = Convert.ToInt32(Console.ReadLine());
            var student = db.Students.Find(id);
            if (student != null)
            {
                Console.Write("Enter new name: ");
                string name = Console.ReadLine();
                if (!string.IsNullOrEmpty(name))
                {
                    student.Name = name;
                }
                Console.Write("Enter new age: ");
                int age;
                if (int.TryParse(Console.ReadLine(), out age))
                {
                    student.Age = age;
                }
                Console.Write("Enter new major: ");
                string major = Console.ReadLine();
                if (!string.IsNullOrEmpty(major))
                {
                    student.Major = major;
                }
                db.SaveChanges();
                Console.WriteLine("Student updated successfully!");
            }
            else
            {
                Console.WriteLine("Student not found!");
            }
            Console.ReadKey();
        }

        public void DeleteStudent()
        {
            Console.Clear();
            Console.WriteLine("Delete Student");
            Console.Write("Enter student id: ");
            int id = Convert.ToInt32(Console.ReadLine());
            var student = db.Students.Find(id);
            if (student != null)
            {
                db.Students.Remove(student);
                db.SaveChanges();
                Console.WriteLine("Student deleted successfully!");
            }
            else
            {
                Console.WriteLine("Student not found!");
            }
            Console.ReadKey();
        }

        public void SearchStudents()
        {
            Console.Clear();
            Console.WriteLine("Search Students");
            Console.Write("Enter student name: ");
            string name = Console.ReadLine();

            var students = from s in db.Students
                           where s.Name.Contains(name)
                           select s;

            foreach (var student in students)
            {
                Console.WriteLine("{0} - {1} - {2} - {3}", student.Name, student.Age, student.Major, student.Gender);
            }
            Console.ReadKey();
        }

        public void Statistics()
        {
            Console.Clear();
            Console.WriteLine("Statistics");
            int totalStudents = db.Students.Count();
            int maleStudents = db.Students.Count(s => s.Gender == "M");
            int femaleStudents = db.Students.Count(s => s.Gender == "F");
            Console.WriteLine("Total Students: {0}", totalStudents);
            Console.WriteLine("Male Students: {0} ({1}%)", maleStudents, (double)maleStudents / totalStudents * 100);
            Console.WriteLine("Female Students: {0} ({1}%)", femaleStudents, (double)femaleStudents / totalStudents * 100);
            Console.ReadKey();
        }
    }

    
}