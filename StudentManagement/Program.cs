﻿using StudentManagement;

try
{
    using (var db = new StudentContext())
    {
        var manager = new StudentManager(db);
        bool exit = false;
        while (!exit)
        {
            Console.Clear();
            Console.WriteLine("Student Management System");
            Console.WriteLine("1. Add Student");
            Console.WriteLine("2. View Students");
            Console.WriteLine("3. Edit Student");
            Console.WriteLine("4. Delete Student");
            Console.WriteLine("5. Search Students");
            Console.WriteLine("6. Statistics");
            Console.WriteLine("7. Exit");
            Console.Write("Enter your choice: ");
            int choice = Convert.ToInt32(Console.ReadLine());
            switch (choice)
            {
                case 1:
                    manager.AddStudent();
                    break;
                case 2:
                    manager.ViewStudents();
                    break;
                case 3:
                    manager.EditStudent();
                    break;
                case 4:
                    manager.DeleteStudent();
                    break;
                case 5:
                    manager.SearchStudents();
                    break;
                case 6:
                    manager.Statistics();
                    break;
                case 7:
                    exit = true;
                    break;
                default:
                    Console.WriteLine("Invalid choice!");
                    break;
            }
        }
    }
}
catch (Exception e)
{

    Console.WriteLine(e);
}
