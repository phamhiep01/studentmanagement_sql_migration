﻿using System.ComponentModel.DataAnnotations;
namespace StudentManagement
{
    public class Student
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public int Age { get; set; }
        public string Major { get; set; }
        public string Gender { get; set; }
    }

    
}