﻿using Microsoft.EntityFrameworkCore;
using System;

namespace StudentManagement
{
    public class StudentContext : DbContext
    {
        public DbSet<Student> Students { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connect = "Data Source=DESKTOP-9FP7ODG;Database=StudentManagement;Trusted_Connection=True;TrustServerCertificate=True";
            optionsBuilder.UseSqlServer(connect);
        }

    }

}